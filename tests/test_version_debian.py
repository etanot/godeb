import pytest

from godeb.version import debian


def test_normalize():
    # Rolled back package containing epoch and Debian revision.
    assert debian.normalize("1:0.7.3+really0.7.2-1") == ("0.7.2", "", "", "")

    # Package contains different Debian revisions and extended versions.
    assert debian.normalize("0.0.0-1.1") == ("0.0.0", "", "", "")
    assert debian.normalize("0.0.1-2+b17") == ("0.0.1", "", "", "")
    assert debian.normalize("10.20.30+dfsg1-2") == ("10.20.30", "", "", "")
    assert debian.normalize("1.0.0+ds1-1+b1") == ("1.0.0", "", "", "")
    assert debian.normalize("0.18.0+ds.1-2") == ("0.18.0", "", "", "")
    assert debian.normalize("0.9.4~ds1-1") == ("0.9.4", "", "", "")

    # Package contains different pre-release tags.
    assert debian.normalize("0.7.3-alpha~dfsg-1") == ("0.7.3", "alpha", "", "")
    assert debian.normalize("2.0.0~beta3-1") == ("2.0.0", "beta3", "", "")
    assert debian.normalize("1.4.5+patch1-1") == ("1.4.5", "patch1", "", "")
    assert debian.normalize("0.0~promoted-339-1.1") == ("0.0.0", "promoted-339", "", "")
    assert debian.normalize("1.6.7~rc0-1") == ("1.6.7", "rc0", "", "")

    # Rolled back package containing epoch, snapshot number and Debian
    # revision.
    assert debian.normalize(
        "1:0.0~git20170407.0.55a552f+really0.0~git20161012.0.5f31782-1+deb8u1"
    ) == ("0.0.0", "", "20161012", "5f31782")

    # Packaging containing different pre-releases and extended versions.
    assert debian.normalize("1:0.0~git20160709.0.0f6e3d1~dfsg-1") == (
        "0.0.0",
        "",
        "20160709",
        "0f6e3d1",
    )
    assert debian.normalize("0.0.1+git20210805.aaa1db6+dfsg1-3") == (
        "0.0.1",
        "",
        "20210805",
        "aaa1db6",
    )
    assert debian.normalize("2.0.0~alpha3~git20190910.7cb4b85+dfsg.1-1") == (
        "2.0.0",
        "alpha3",
        "20190910",
        "7cb4b85",
    )
    assert debian.normalize("10.20.30~git20160709.0.0f6e3d1~ds1-3") == (
        "10.20.30",
        "",
        "20160709",
        "0f6e3d1",
    )
    assert debian.normalize("99.99.99-rc.4+git20170922.c385f95~ds.1-1") == (
        "99.99.99",
        "rc.4",
        "20170922",
        "c385f95",
    )
    assert debian.normalize("99.99.99-patch1+git20170922.c385f95+ds.1-1+b6") == (
        "99.99.99",
        "patch1",
        "20170922",
        "c385f95",
    )
    assert debian.normalize("99.99.99~promoted-339+git20170922.c385f95+ds.1-1") == (
        "99.99.99",
        "promoted-339",
        "20170922",
        "c385f95",
    )

    # Revision length is greater than 7. An example of
    # "golang-github-dgryski-go-farm" package.
    assert debian.normalize("0.0.1~git20171119.ac7624ea8da3-1.1") == (
        "0.0.1",
        "",
        "20171119",
        "ac7624ea8da3",
    )

    # Could be a valid Debian version, but not supported by us.
    assert debian.normalize("1-2") == None
    assert debian.normalize("1.0~alpha3-1") == None
    assert debian.normalize("0.9-1") == None  # golang-github-asaskevich-govalidator
    assert (
        debian.normalize("9+git20180720.0.f9ffefc3-1.1") == None
    )  # golang-github-asaskevich-govalidator
    assert debian.normalize("0.1+git20200305.7d891c7-5") == None
    assert debian.normalize("1.0~alpha3+git20170922.c385f95-1") == None

    # Invalid version (version not following Go team's packaging guideline
    # strongly), but somehow program still works.
    # TODO: Require some logic fix in the program.
    debian.normalize("1.0.0+20200724-1") == ("1.0.0", "20200724", "", "")

    # Invalid versions. Versions which either don't follow Go team's packaging
    # guideline strongly or they are way before the guideline itself.
    assert debian.normalize("0~20150201-3") == None
    assert debian.normalize("0.0~git20150730-2") == None
    assert debian.normalize("0.4+git256976a-2") == None
    assert debian.normalize("0.0.0+2016.01.15.git.29cc9e1b05-2+b6") == None
    assert debian.normalize("1.4.39.g2972be2-3") == None
    assert debian.normalize("2016.08.01-7") == None
    assert debian.normalize("1~bzr20150122-3") == None
    assert (
        debian.normalize(
            "1:0.0~git20170407.0.55a552f+REALLY.0.0~git20161012.0.5f31782-1+deb8u1"
        )
        == None
    )

    # Invalid base versions. For more examples, see:
    # https://regex101.com/r/Ly7O1x/3/.
    assert debian.normalize("01.1.1") == None
    assert debian.normalize("1.01.1") == None
    assert debian.normalize("1.1.01") == None
    assert debian.normalize("1.2.3.DEV") == None
    assert debian.normalize("1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12") == None
